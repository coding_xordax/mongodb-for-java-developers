package com.merukeru.springuniversity.week1;

import freemarker.template.Configuration;
import freemarker.template.Template;
import spark.Request;
import spark.Response;
import spark.Spark;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import static spark.Spark.halt;

/**
 * Created by merukeru on 6/17/17.
 */
public class HelloWorld {

    private final static Configuration cfg;

    static {
        cfg = new Configuration(Configuration.VERSION_2_3_23);
        cfg.setClassForTemplateLoading(HelloWorld.class, "/");
    }

    private static StringWriter handleGetRequest(Request req, Response res) {
        StringWriter writer = new StringWriter();
        if(cfg == null) {
            return writer;
        }
        try {
            Template helloTemplate = cfg.getTemplate("hello.ftl");
            Map<String, Object> helloMap = new HashMap<String, Object>();
            helloMap.put("name", "Freemarker");

            helloTemplate.process(helloMap, writer);
        } catch (Exception e) {
            halt(500);
            e.printStackTrace();
        }
        return writer;
    }

    public static void main(String[] args) {
        Spark.get("/hello", (req, res) -> handleGetRequest(req, res));
    }
}
