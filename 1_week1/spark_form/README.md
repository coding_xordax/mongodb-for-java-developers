
# Spark form example

## Links

[http://sparkjava.com](http://sparkjava.com)
[https://github.com/perwendel/spark](https://github.com/perwendel/spark)

[http://freemarker.org/](http://freemarker.org/)
[https://github.com/apache/incubator-freemarker](https://github.com/apache/incubator-freemarker)

## Run

1. Start Application from IDE
1. Open Browser [http://localhost:4567/](http://localhost:4567/)


