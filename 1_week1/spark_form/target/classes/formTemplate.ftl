<html>
  <head><title>Movie Picker</title></head>
  <body>
     <form action="/favorite_movie" method="POST">
        <p>What is your favorite movie?</p>
        <#list movies as movie>
          <p>
             <input type="radio" name="movie" value="${movie}">${movie}</input>
          </p>
        </#list>
        <input type="submit" value="Submit"/>
     </form>
  </body>
</html>