package com.merukeru.springuniversity.week1;

/**
 * Created by merukeru on 6/18/17.
 */
public class Application {
    public static void main(String[] args) {
        RequestHandler handler = new RequestHandler();
        handler.handle();
    }
}
