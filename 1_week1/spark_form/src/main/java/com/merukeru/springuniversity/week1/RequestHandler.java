package com.merukeru.springuniversity.week1;

import freemarker.template.Configuration;
import freemarker.template.Template;
import spark.Request;
import spark.Response;
import spark.Spark;

import java.io.StringWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by merukeru on 6/18/17.
 */
public class RequestHandler {

    private Configuration cfg;
    private StringWriter writer;

    public RequestHandler() {
        this.cfg = new Configuration(Configuration.VERSION_2_3_23);
        this.cfg.setClassForTemplateLoading(Application.class, "/");
        this.writer = new StringWriter();
    }

    public void handle() {
        Spark.get("/", (req, res) -> handleGetRequest(req, res));
        Spark.post("/favorite_movie", (req, res) -> handlePostRequest(req, res));
    }

    private Object handlePostRequest(Request req, Response res) {
        final String movie = req.queryParams("movie");
        if (movie == null) {
            return "Why don't you pick one?";
        }
        else {
            return "Your favorite movie is " + movie;
        }
    }

    private Object handleGetRequest(Request req, Response res) {
        try {
            Map<String, Object> moviesMap = new HashMap<String, Object>();
            moviesMap.put("movies",
                    Arrays.asList("Iron Man", "A Clockwork Orange", "Pulp Fiction", "Watchmen"));

            Template moviePickerTemplate =
                    cfg.getTemplate("formTemplate.ftl");
            StringWriter writer = new StringWriter();
            moviePickerTemplate.process(moviesMap, writer);
            return writer;
        } catch (Exception e) {
            Spark.halt(500);
            return null;
        }
    }
}
