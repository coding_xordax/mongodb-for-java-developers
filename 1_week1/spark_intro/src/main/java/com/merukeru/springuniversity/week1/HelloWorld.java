package com.merukeru.springuniversity.week1;

import spark.Spark;

/**
 * Created by merukeru on 6/17/17.
 */
public class HelloWorld {
    public static void main(String[] args) {
        Spark.get("/hello", (req, res) -> "Hello World From Spark");
    }
}
