package com.merukeru.springuniversity.week1;

import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by merukeru on 6/17/17.
 */
public class HelloWorld {
    public static void main(String[] args) {
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);

        cfg.setClassForTemplateLoading(
                HelloWorld.class, "/");

        cfg.setDefaultEncoding("UTF-8");

        try {
            Template helloTemplate = cfg.getTemplate("hello.ftl");
            StringWriter writer = new StringWriter();
            Map<String, Object> helloMap = new HashMap<String, Object>();
            helloMap.put("name", "Freemarker");

            helloTemplate.process(helloMap, writer);

            System.out.println(writer);

        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
