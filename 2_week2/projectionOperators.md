# mongodb projection operators

```
> db.movieDetails.findOne( { "year": { $gt: 2015 } }, { "year": 1 } )
{ "_id" : ObjectId("5692a27324de1e0ce2dfd701"), "year" : 2016 }
```

```
> db.movieDetails.findOne( { "year": { $gt: 2015 } }, { "year": 1, "_id": 0 } )
{ "year" : 2016 }
```