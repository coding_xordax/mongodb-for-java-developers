# mongodb regex operators

```
> db.movieDetails.find({ "awards.text": { $regex: /^Won.*/ } }).count()
83
```

```
> db.movieDetails.find({ "awards.text": { $regex: /^Won.*/ } }, { title: 1, _id: 0}).pretty()
{ "title" : "West Side Story" }
{ "title" : "How the West Was Won" }
{ "title" : "Star Wars: Episode IV - A New Hope" }
{ "title" : "Star Wars: Episode V - The Empire Strikes Back" }
{ "title" : "Star Trek" }
{ "title" : "Shakespeare in Love" }
{ "title" : "2001: A Space Odyssey" }
{ "title" : "The Adventures of Robin Hood" }
{ "title" : "The Adventures of Priscilla, Queen of the Desert" }
{ "title" : "The Last Picture Show" }
{ "title" : "The Greatest Show on Earth" }
{ "title" : "Pirates of the Caribbean: Dead Man's Chest" }
{ "title" : "Dead Poets Society" }
{ "title" : "Alien" }
{ "title" : "Butch Cassidy and the Sundance Kid" }
{ "title" : "All Quiet on the Western Front" }
{ "title" : "All Quiet on the Western Front" }
{ "title" : "Midnight Cowboy" }
{ "title" : "The Cowboy and the Lady" }
{ "title" : "Top Gun" }
Type "it" for more
```