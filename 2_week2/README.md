# Import Sample Data

1. start mongodb
```sh
$ docker-compose up -d
```

1. check if mongo is started
```sh
$ docker-compose ps
```

1. connect to mongo
```sh
$ docker exec -it mongo bash
```

1. restore dump
```sh
$ cd scripts
$ ./import_week2_dump.sh
```