# mongodb update operators

## update fields
[https://docs.mongodb.com/manual/reference/operator/update-field/](https://docs.mongodb.com/manual/reference/operator/update-field/)

`$set`          Add a new field to a document or update a field.

`$unset`        Removes a field.

`$inc`          Increase (positive value) and decrease (negative value) a field by a specific amount. 

`$mul`          Multiplies the value of the field by the specified amount.

`$rename`       Renames a field of a document.

`$setOnInsert`  Sets the value of a field if an update results in an insert of a document. Has no effect on update operations that modify existing documents.

`$min`          Only updates the field if the specified value is greater than the existing field value.

`$max`          Only updates the field if the specified value is less than the existing field value.

`$currentDate`  Sets the value of a field to current date, either as a Date or a Timestamp.

## update arrays
[https://docs.mongodb.com/manual/reference/operator/update-array/](https://docs.mongodb.com/manual/reference/operator/update-array/)

`$addToSet`     Adds elements to an array only if they do not already exist in the set.

`$pop`          Removes the first or last item of an array.

`$push`         Adds an item to an array.

`$pull`         Removes all array elements that match a specified query.

`$pullAll`      Removes all matching values from an array.

`$each`         With this flag the `$push` and `$addToSet` operators append multiple items for array updates.

`$position`     Modifies the `$push` operator to specify the position in the array to add elements.

`$slice`        Modifies the `$push` operator to limit the size of updated arrays.

`$sort`         Modifies the `$push` operator to reorder documents stored in an array.

`upsert`        Creates a new document when no document matches the query criteria.