# mongodb element operators

```
> db.movieDetails.find( { "year": { $exists: true } } ).count()
2295
```

```
> db.movieDetails.find( { "year": { $exists: false } } ).count()
0
```

```
> db.movieDetails.find( { _id: { $type: "string" } } ).count()
0
```

```
> db.movieDetails.find( { year: { $type: "number" } } ).count()
2295
```