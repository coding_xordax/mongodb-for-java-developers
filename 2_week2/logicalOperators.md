# mongodb logical operators

```
> db.movieDetails.find({$or:[{"tomato.meter":{$gt:99}},{"metacritic":{$gt:95}}]}).count()
11
```

```
> db.movieDetails.find({$and:[{"metacritic":{$ne:100}},{"metacritic":{$exists:false}}]}).count()
1919
```