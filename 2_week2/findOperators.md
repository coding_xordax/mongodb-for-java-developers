# mongodb find operators

## get databases and collections

```
> show dbs
admin  0.000GB
local  0.000GB
video  0.000GB
```

```
> use video
switched to db video
```

```
> show collections
movieDetails
movies
moviesScratch
reviews
```

## find and count

```
> db.movies.find().count()
3365
```

```
> db.movies.findOne()
{
        "_id" : ObjectId("56918f5e24de1e0ce2dfcccd"),
        "title" : "Star Wars: Episode IV - A New Hope",
        "year" : 1977,
        "imdb" : "tt0076759",
        "type" : "movie"
}
```

## exact match

```
> db.movieDetails.find({"genres":["Comedy"]}).count()
165
```

```
> db.movieDetails.find({"genres":["Comedy", "Crime"]}).count()
20
```

```
> db.movieDetails.find({"genres":["Crime", "Comedy"]}).count()
0
```

## $in and $all

$in = matches one element from a given array
$all = matches all elements from a given array
$nin = matches none element from a given array (not in)
$elemMatch = matches at least one element that matches all the specified query criteria

```
> db.movieDetails.find({"genres": { $in: ["Crime", "Comedy"]}}).count()
874
```

```
> db.movieDetails.find({ "genres": { $in: ["Comedy", "Crime", "Drama"] } }).count()
1311
```

```
> db.movieDetails.find({ "genres": { $all: ["Comedy", "Crime", "Drama"] } }).count()
8
```

## comparison operators

$gt = greater than
$gte = greater than or equal to
$lt = less than
$eq = equals
$lte = is less than or equal to
$ne =  not equal

$size = array size

```
> db.movieDetails.find( { "year": { $gt: 2000 } } ).count()
1272
```

```
> db.movieDetails.find( { "year": { $gt: 2015 } } ).count()
9
```

```
> db.movieDetails.findOne( { "year": { $gt: 2015 } } )
{
        "_id" : ObjectId("5692a27324de1e0ce2dfd701"),
        "title" : "Ég Man Þig",
        "year" : 2016,
        "rated" : null,
        "released" : null,
        "runtime" : null,
        "countries" : [
                "Iceland"
        ],
        "genres" : [
                "Thriller"
        ],
        "director" : "Óskar Thór Axelsson",
        "writers" : [
                "Óskar Thór Axelsson",
                "Ottó Geir Borg"
        ],
        "actors" : [ ],
        "plot" : null,
        "poster" : null,
        "imdb" : {
                "id" : "tt4966532",
                "rating" : null,
                "votes" : null
        },
        "awards" : {
                "wins" : 0,
                "nominations" : 0,
                "text" : ""
        },
        "type" : "movie"
}
```

