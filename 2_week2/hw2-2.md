
```
> use students
switched to db students
```

```
> db.grades.count()
800
```

```
> db.grades.aggregate({'$group':{'_id':'$student_id', 'average':{$avg:'$score'}}}, {'$sort':{'average':-1}}, {'$limit':1})
{ "_id" : 164, "average" : 89.29771818263373 }
```

```
> db.grades.findOne()
{
        "_id" : ObjectId("50906d7fa3c412bb040eb577"),
        "student_id" : 0,
        "type" : "exam",
        "score" : 54.6535436362647
}
```

```
> db.grades.find({"score": {"$gte": 65}}).limit(10)
{ "_id" : ObjectId("50906d7fa3c412bb040eb57b"), "student_id" : 1, "type" : "exam", "score" : 74.20010837299897 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb57c"), "student_id" : 1, "type" : "quiz", "score" : 96.76851542258362 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb582"), "student_id" : 2, "type" : "homework", "score" : 97.75889721343528 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb583"), "student_id" : 3, "type" : "exam", "score" : 92.6244233936537 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb584"), "student_id" : 3, "type" : "quiz", "score" : 82.59760859306996 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb586"), "student_id" : 3, "type" : "homework", "score" : 92.71871597581605 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb587"), "student_id" : 4, "type" : "exam", "score" : 87.89071881934647 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb58f"), "student_id" : 6, "type" : "exam", "score" : 97.56302189646776 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb591"), "student_id" : 6, "type" : "homework", "score" : 81.23822046161325 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb592"), "student_id" : 6, "type" : "homework", "score" : 89.72700715074382 }
```

```
> db.grades.find({"score": {"$gte": 65}}).sort({"score": 1}).limit(10)
{ "_id" : ObjectId("50906d7fa3c412bb040eb5cf"), "student_id" : 22, "type" : "exam", "score" : 65.02518811936324 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb70a"), "student_id" : 100, "type" : "homework", "score" : 65.29214756759019 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb676"), "student_id" : 63, "type" : "homework", "score" : 65.31038121884853 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb77a"), "student_id" : 128, "type" : "homework", "score" : 65.47002803265133 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb743"), "student_id" : 115, "type" : "exam", "score" : 65.47329199925679 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb80c"), "student_id" : 165, "type" : "quiz", "score" : 65.54110645268801 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb695"), "student_id" : 71, "type" : "homework", "score" : 65.54625488975057 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb637"), "student_id" : 48, "type" : "exam", "score" : 65.71867938396781 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb6fc"), "student_id" : 97, "type" : "quiz", "score" : 65.90504076423 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb65b"), "student_id" : 57, "type" : "exam", "score" : 65.91867871499709 }
```

```
> db.grades.find({"score": {"$gte": 65}, "type":{"$eq":"exam"}}).sort({"score": 1}).limit(10)
{ "_id" : ObjectId("50906d7fa3c412bb040eb5cf"), "student_id" : 22, "type" : "exam", "score" : 65.02518811936324 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb743"), "student_id" : 115, "type" : "exam", "score" : 65.47329199925679 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb637"), "student_id" : 48, "type" : "exam", "score" : 65.71867938396781 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb65b"), "student_id" : 57, "type" : "exam", "score" : 65.91867871499709 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb6d3"), "student_id" : 87, "type" : "exam", "score" : 66.0470217410135 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb87f"), "student_id" : 194, "type" : "exam", "score" : 67.09136149008972 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb747"), "student_id" : 116, "type" : "exam", "score" : 67.09938431313856 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb733"), "student_id" : 111, "type" : "exam", "score" : 67.16752597563053 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb893"), "student_id" : 199, "type" : "exam", "score" : 67.33828604577803 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb6f7"), "student_id" : 96, "type" : "exam", "score" : 67.39154510277987 }
```

```
> db.grades.find( { score : { $gte : 65 }, type: "exam" } ).sort( { score : 1 } ).limit(1)
{ "_id" : ObjectId("50906d7fa3c412bb040eb5cf"), "student_id" : 22, "type" : "exam", "score" : 65.02518811936324 }
```
