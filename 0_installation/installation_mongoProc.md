# Install Mongo Academy mongoProc

## Installation

1. Download Linux package of mongoProc:
`https://university.mongodb.com/mongoproc`

1. Extract archive and move it

1. Run mongoProc from mongoProc directory
```
$ ./mognoProc.sh
```

## Errors

### Missing libncurses.so.5
```#
[...] error while loading shared libraries: libncurses.so.5: cannot open shared object file: No such file or directory [...]
```

Update system
```sh
$ pacaur -Syu
```

Install ncurses5
```
$ pacaur -S lib32-ncurses ncurses ncurses5-compat-libs
```

If a gpg key could not be verified add key and retry installation step again.
```sh
$ gpg --recv-keys 702353E0F7E48EDB
$ pacaur -S lib32-ncurses ncurses ncurses5-compat-libs
```
