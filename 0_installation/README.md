# Week 1: Installation in Linux (Arch LInux)

## Installation

```sh
$ sudo pacman -Syu
$ sudo pacman -S mongodb mongodb-tools
$ systemctl enable mongod
$ systemctl sart mongod
$ systemctl status mongod
```

Run mongo shell
```sh
$ mongo
MongoDB shell version v3.4.3
connecting to: mongodb://127.0.0.1:27017
MongoDB server version: 3.4.5
>
```

## Run MongoDB via docker

see configuration in `docker-compose` file

Run MongoDB via docker-compose
```sh
$ docker-compose up
```

Run in background (Detached mode):
```sh
$ docker-compose build
$ docker-compose up -d
```

Connect to docker container
(exit with ctrl+c)
```sh
$ docker exec -it mongo bash
```

Run mongo shell
```sh
$ mongo
MongoDB shell version v3.4.5
connecting to: mongodb://127.0.0.1:27017
MongoDB server version: 3.4.5
Welcome to the MongoDB shell.
>
```

## Use the mongo shell
```json
> show dbs
admin  0.000GB
local  0.000GB
```

```json
> use video
switched to db video
```

```json
> db.movies.insertOne({ "title": "Iron Man", "year": 2008, "imdb": "tt0371746" });
{
        "acknowledged" : true,
        "insertedId" : ObjectId("594434735d1faebea4b3b0e0")
}
```
with this insert command the database `video` and the collection `movies` is created

```json
> show dbs
admin  0.000GB
local  0.000GB
video  0.000GB
```

```json
> db.movies.insertOne({ "title": "Thor", "year": 2011, "imdb": "tt0800369" })
{
        "acknowledged" : true,
        "insertedId" : ObjectId("5944348d5d1faebea4b3b0e1")
}
```

```json
> db.movies.insertOne({ "title": "Man of Steel", "year": 2013, "imdb": "tt0770828" })
{
        "acknowledged" : true,
        "insertedId" : ObjectId("594434935d1faebea4b3b0e2")
}
```

```json
> db.movies.find()
{ "_id" : ObjectId("594434735d1faebea4b3b0e0"), "title" : "Iron Man", "year" : 2008, "imdb" : "tt0371746" }
{ "_id" : ObjectId("5944348d5d1faebea4b3b0e1"), "title" : "Thor", "year" : 2011, "imdb" : "tt0800369" }
{ "_id" : ObjectId("594434935d1faebea4b3b0e2"), "title" : "Man of Steel", "year" : 2013, "imdb" : "tt0770828" }
```

Find a movie by name
```json
> db.movies.find( {"title":"Iron Man"})
{ "_id" : ObjectId("594434735d1faebea4b3b0e0"), "title" : "Iron Man", "year" : 2008, "imdb" : "tt0371746" }
```

## Run script

Create a javascript file (or use insert_movies.js)
```json
> use video
> db.movies.insertOne({ "title": "Iron Man", "year": 2008, "imdb": "tt0371746" })
> db.movies.insertOne({ "title": "Thor", "year": 2011, "imdb": "tt0800369" })
> db.movies.insertOne({ "title": "Man of Steel", "year": 2013, "imdb": "tt0770828" })
```

Run script via mongo 
```sh
$ mongo < insert_movies.js
MongoDB shell version v3.4.5
connecting to: mongodb://127.0.0.1:27017
MongoDB server version: 3.4.5
switched to db video

delete movies
{ "acknowledged" : true, "deletedCount" : 3 }

add movies
{
        "acknowledged" : true,
        "insertedId" : ObjectId("594520827b78c8e317f44bf3")
}
{
        "acknowledged" : true,
        "insertedId" : ObjectId("594520827b78c8e317f44bf4")
}
{
        "acknowledged" : true,
        "insertedId" : ObjectId("594520827b78c8e317f44bf5")
}

get movies
{ "_id" : ObjectId("594520827b78c8e317f44bf3"), "title" : "Iron Man", "year" : 2008, "imdb" : "tt0371746" }
{ "_id" : ObjectId("594520827b78c8e317f44bf4"), "title" : "Thor", "year" : 2011, "imdb" : "tt0800369" }
{ "_id" : ObjectId("594520827b78c8e317f44bf5"), "title" : "Man of Steel", "year" : 2013, "imdb" : "tt0770828" }
bye
```

