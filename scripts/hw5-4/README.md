# hw5-4

## prepare db

download the provided `zips.json` or use it from repo

start mongo database: `docker-compose up -d`

connect to docker container: `docker exec -it mongo bash`

go to `hw5-4` directory: `cd scripts/hw5-4`

prepare database: `mongoimport --drop -d test -c zips zips.json`

start mongo shell: `mongo`

use database: `use test`

## hints

Get some information on the collection and documents

```
> db.zips.count()
29467
```
```
> db.zips.find().limit(2).pretty()
{
        "_id" : "35004",
        "city" : "ACMAR",
        "loc" : [
                -86.51557,
                33.584132
        ],
        "pop" : 6055,
        "state" : "AL"
}
{
        "_id" : "35005",
        "city" : "ADAMSVILLE",
        "loc" : [
                -86.959727,
                33.588437
        ],
        "pop" : 10616,
        "state" : "AL"
}
```

Extract first character of city
```
> db.zips.aggregate([
... {$project: { first_char: {$substr : ["$city",0,1]}, pop: "$pop"}},
... {$limit: 3}
... ])
{ "_id" : "35004", "first_char" : "A", "pop" : 6055 }
{ "_id" : "35005", "first_char" : "A", "pop" : 10616 }
{ "_id" : "35006", "first_char" : "A", "pop" : 3205 }
```

Get rid of all documents where the city does not start with a digit (0-9)
```
> db.zips.aggregate([
... {$project: { first_char: {$substr : ["$city",0,1]}, pop: "$pop"}},
... {$match: {"first_char": {$in:['0','1','2','3','4','5','6','7','8','9']}}},
... {$limit: 3}
... ])
{ "_id" : "98791", "first_char" : "9", "pop" : 5345 }
{ "_id" : "95411", "first_char" : "9", "pop" : 133 }
{ "_id" : "95414", "first_char" : "9", "pop" : 226 }
```

Group results by first_char
```
> db.zips.aggregate([
... {$project: { first_char: {$substr : ["$city",0,1]}, pop: "$pop"}},
... {$match: {"first_char": {$in:['0','1','2','3','4','5','6','7','8','9']}}},
... {$group: {"_id" : 1, sum: {$sum:"$pop"}}}
... ])
{ "_id" : 1, "sum" : 298015 }
```
