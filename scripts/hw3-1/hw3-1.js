use school;

var MAX_SCORE = 99999;
var START_INDEX = -1;

print("\nNumber of students:")
db.students.count();

db.students.find().forEach(
    function(student) {
        var scores = student.scores;
        var scoresCount = student.scores.length;
        var hwIndex = START_INDEX;
        var hwScore = MAX_SCORE;

        print("\n student " + student._id + " has " + scoresCount + " scores")

        for(var iter = 0; iter < scoresCount; iter++) {
            if (scores[iter].type == "homework" && scores[iter].score < hwScore) {
                hwIndex = iter;
                hwScore = scores[iter].score;
            }
        }

        print(" - hwIndex: " + hwIndex);
        print(" - hwScore: " + hwScore);

        if (hwIndex > START_INDEX && hwScore < MAX_SCORE) {
            print(" - remove score: " + JSON.stringify(scores[hwIndex], null, 2));
            scores.splice(hwIndex, 1);
            db.students.update({_id: student._id},{$set:{'scores': scores}});
        }
    }
);

print("\nNumber of students:");
db.students.count();

print("\nStudent with _id = 137:");
db.students.find({_id:137}).pretty()

print("\nFind student with the highest average");
db.students.aggregate( [
  { '$unwind': '$scores' },
  {
    '$group':
    {
      '_id': '$_id',
      'average': { $avg: '$scores.score' }
    }
  },
  { '$sort': { 'average' : -1 } },
  { '$limit': 1 } ] )