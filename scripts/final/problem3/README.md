# problem3

download the provided `final3_validate_mongo_shell.js` or use it from repo

start mongo database: `docker-compose up -d`

connect to docker container: `docker exec -it mongo bash`

go to `problem3` directory: `cd scripts/final/problem3`

start mongo shell: `mongo`

```
> db.messages.update({ 'headers.Message-ID': '<8147308.1075851042335.JavaMail.evans@thyme>' }, { $addToSet: { 'headers.To': 'mrpotatohead@mongodb.com' } })
WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })
> db.messages.find ({ 'headers.Message-ID': '<8147308.1075851042335.JavaMail.evans@thyme>'},{body:0}).pretty()
{
        "_id" : ObjectId("4f16fd52d1e2d32371039e44"),
        "filename" : "1646.",
        "headers" : {
                "Content-Transfer-Encoding" : "7bit",
                "Content-Type" : "text/plain; charset=us-ascii",
                "Date" : ISODate("2001-07-13T14:47:00Z"),
                "From" : "donna.fulton@enron.com",
                "Message-ID" : "<8147308.1075851042335.JavaMail.evans@thyme>",
                "Mime-Version" : "1.0",
                "Subject" : "RTO Orders - Grid South, SE Trans, SPP and Entergy",
                "To" : [
                        "steven.kean@enron.com",
                        "richard.shapiro@enron.com",
                        "james.steffes@enron.com",
                        "christi.nicolay@enron.com",
                        "sarah.novosel@enron.com",
                        "ray.alvarez@enron.com",
                        "sscott3@enron.com",
                        "joe.connor@enron.com",
                        "dan.staines@enron.com",
                        "steve.montovano@enron.com",
                        "kevin.presto@enron.com",
                        "rogers.herndon@enron.com",
                        "mike.carson@enron.com",
                        "john.forney@enron.com",
                        "laura.podurgiel@enron.com",
                        "gretchen.lotz@enron.com",
                        "juan.hernandez@enron.com",
                        "miguel.garcia@enron.com",
                        "rudy.acevedo@enron.com",
                        "heather.kroll@enron.com",
                        "david.fairley@enron.com",
                        "elizabeth.johnston@enron.com",
                        "bill.rust@enron.com",
                        "edward.baughman@enron.com",
                        "terri.clynes@enron.com",
                        "oscar.dalton@enron.com",
                        "doug.sewell@enron.com",
                        "larry.valderrama@enron.com",
                        "nick.politis@enron.com",
                        "fletcher.sturm@enron.com",
                        "chris.dorland@enron.com",
                        "jeff.king@enron.com",
                        "john.kinser@enron.com",
                        "matt.lorenz@enron.com",
                        "patrick.hansen@enron.com",
                        "lloyd.will@enron.com",
                        "dduaran@enron.com",
                        "john.lavorato@enron.com",
                        "louise.kitchen@enron.com",
                        "greg.whalley@enron.com",
                        "mrpotatohead@mongodb.com"
                ],
                "X-FileName" : "skean.nsf",
                "X-Folder" : "\\Steven_Kean_Oct2001_2\\Notes Folders\\Attachments",
                "X-From" : "Donna Fulton",
                "X-Origin" : "KEAN-S",
                "X-To" : "Steven J Kean, Richard Shapiro, ...",
                "X-bcc" : "",
                "X-cc" : ""
        },
        "mailbox" : "kean-s",
        "subFolder" : "attachments"
}
```

start mongo with validation script: `mongo final3_validate_mongo_shell.js`

```
$ mongo final3_validate_mongo_shell.js
MongoDB shell version v3.4.3
connecting to: mongodb://127.0.0.1:27017
MongoDB server version: 3.4.5
Welcome to the Final Exam Q3 Checker. My job is to make sure you correctly updated the document
Final Exam Q3 Validated successfully!
Your validation code is: vOnRg05kwcqyEFSve96R
```


