# final

## prepare db

download the provided `enron.zip` or use it from repo

start mongo database: `docker-compose up -d`

connect to docker container: `docker exec -it mongo bash`

go to `final` directory: `cd scripts/final`

prepare database:
```
# mongorestore -d enron -c messages ./enron/messages.bson
2017-07-12T10:18:06.104+0000    checking for collection data in enron/messages.bson
2017-07-12T10:18:06.105+0000    reading metadata for enron.messages from enron/messages.metadata.json
2017-07-12T10:18:06.753+0000    restoring enron.messages from enron/messages.bson
2017-07-12T10:18:09.045+0000    [##########..............]  enron.messages  159MB/378MB  (42.1%)
2017-07-12T10:18:12.042+0000    [###################.....]  enron.messages  302MB/378MB  (80.0%)
2017-07-12T10:18:15.042+0000    [########################]  enron.messages  378MB/378MB  (100.0%)
2017-07-12T10:18:18.041+0000    [########################]  enron.messages  378MB/378MB  (100.0%)
2017-07-12T10:18:21.047+0000    [########################]  enron.messages  378MB/378MB  (100.0%)
2017-07-12T10:18:21.131+0000    [########################]  enron.messages  378MB/378MB  (100.0%)
2017-07-12T10:18:21.131+0000    no indexes to restore
2017-07-12T10:18:21.131+0000    finished restoring enron.messages (120477 documents)
2017-07-12T10:18:21.131+0000    done
```

start mongo shell: `mongo`

use database: `use enron`

get some infos about the db:

```
> show dbs
admin  0.000GB
enron  0.209GB
local  0.000GB
> use enron
switched to db enron
> show collections
messages
> db.messages.count()
120477
> db.messages.findOne()
{
        "_id" : ObjectId("4f16fc98d1e2d323710045e4"),
        "body" : "some long text ...",
        "filename" : "217.",
        "headers" : {
                "Content-Transfer-Encoding" : "7bit",
                "Content-Type" : "text/plain; charset=us-ascii",
                "Date" : ISODate("2001-03-02T15:24:00Z"),
                "From" : "russell.ballato@enron.com",
                "Message-ID" : "<28244517.1075848316070.JavaMail.evans@thyme>",
                "Mime-Version" : "1.0",
                "Subject" : "FW: TGIF",
                "To" : [
                        "don.baughman@enron.com"
                ],
                "X-FileName" : "dbaughm.nsf",
                "X-Folder" : "\\Edward_Baughman_Nov2001\\Notes Folders\\All documents",
                "X-From" : "Russell Ballato",
                "X-Origin" : "BAUGHMAN-E",
                "X-To" : "Don Baughman",
                "X-bcc" : "",
                "X-cc" : ""
        },
        "mailbox" : "baughman-d",
        "subFolder" : "all_documents"
}
```

## problem 1
```
> db.messages.find({"headers.From":"andrew.fastow@enron.com", "headers.To":"jeff.skilling@enron.com"},{body:0}).count();
3
```

## problem 2
```
> db.messages.aggregate([
... {$project:{body:0}},
... // unwind
... {$unwind:"$headers.To"},
... // group by id, from and to fields to remove duplicates
... {$group:{"_id" : {id:"$_id", from:"$headers.From", to:"$headers.To"}}},
... // count the messages
... {$group:{"_id" : {from:"$_id.from", to:"$_id.to"}, sum: {$sum:1}}},
... // sort to get higher counts first
... {$sort: {sum: -1}},
...     {$limit: 3}
... ])
```

I got some error message: "Exceeded memory limit for $group, but didn't allow external sort. Pass allowDiskUse:true to opt in.". Passing the allowDiskUse fixes this error:

```
> db.messages.aggregate([
... {$project:{body:0}},
... // unwind
... {$unwind:"$headers.To"},
... // group by id, from and to fields to remove duplicates
... {$group:{"_id" : {id:"$_id", from:"$headers.From", to:"$headers.To"}}},
... // count the messages
... {$group:{"_id" : {from:"$_id.from", to:"$_id.to"}, sum: {$sum:1}}},
... // sort to get higher counts first
... {$sort: {sum: -1}},
...     {$limit: 3}
... ],
... { "allowDiskUse": true })
{ "_id" : { "from" : "susan.mara@enron.com", "to" : "jeff.dasovich@enron.com" }, "sum" : 750 }
{ "_id" : { "from" : "soblander@carrfut.com", "to" : "soblander@carrfut.com" }, "sum" : 679 }
{ "_id" : { "from" : "susan.mara@enron.com", "to" : "james.steffes@enron.com" }, "sum" : 646 }
```
