# problem4

download the provided `posts_f4_m101j.json` or use it from repo

start mongo database: `docker-compose up -d`

connect to docker container: `docker exec -it mongo bash`

go to `problem4` directory: `cd scripts/final/problem4`

prepare database: `mongoimport --drop -d blog -c posts posts_f4_m101j.json`

start mongo shell: `mongo`
