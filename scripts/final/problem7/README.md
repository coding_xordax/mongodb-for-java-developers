# problem7

download the provided `final7.zip` or use it from repo

start mongo database: `docker-compose up -d`

connect to docker container: `docker exec -it mongo bash`

go to `problem7` directory: `cd scripts/final/problem7`

prepare database: 
```sh
mongoimport --drop -d final7 -c albums ./final7/albums.json
mongoimport --drop -d final7 -c images ./final7/images.json
```

start mongo shell: `mongo`

use database: `use final7`

create index to speed up operations: `db.albums.ensureIndex( { images : 1 } )`