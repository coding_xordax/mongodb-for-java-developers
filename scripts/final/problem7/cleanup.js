use final7;

var orphanImages = [];

print("\nNumber of images:")
db.images.count();

print("\nNumber of albums:")
db.albums.count();

db.images.find( {  } ).forEach(
    function(image) {
        var numAlbumsForImage = db.albums.find({"images":image._id}).count();
        if (numAlbumsForImage <= 0) {
            orphanImages.push(image._id);
        }
    }
);

print("\nNumber of orphanImages: " + orphanImages.length)
orphanImages.forEach(function del(imageId){
    db.images.deleteOne({_id: imageId});
});

print("\nNumber of images:")
db.images.count();