# hw5-2

## prepare db

download the provided `small_zips.json` or use it from repo

start mongo database: `docker-compose up -d`

connect to docker container: `docker exec -it mongo bash`

go to `hw5-2` directory: `cd scripts/hw5-2`

prepare database: `mongoimport --drop -d test -c zips small_zips.json`

start mongo shell: `mongo`

use database: `use test`

## hints

Get some information on the collection and documents
```
> db.zips.find().count()
200
```
```
> db.zips.find().limit(2).pretty()
{
        "_id" : "92278",
        "city" : "TWENTYNINE PALMS",
        "loc" : [
                -116.06041,
                34.237969
        ],
        "pop" : 11412,
        "state" : "CA"
}
{
        "_id" : "93254",
        "city" : "NEW CUYAMA",
        "loc" : [
                -119.823806,
                34.996709
        ],
        "pop" : 80,
        "state" : "CA"
}
```

Use a filter to consider only the states NY and CA
```
> db.zips.aggregate([{$match:{state:{$in:['NY', 'CA']}}},{$limit: 10}])
{ "_id" : "92278", "city" : "TWENTYNINE PALMS", "loc" : [ -116.06041, 34.237969 ], "pop" : 11412, "state" : "CA" }
{ "_id" : "93254", "city" : "NEW CUYAMA", "loc" : [ -119.823806, 34.996709 ], "pop" : 80, "state" : "CA" }
{ "_id" : "92070", "city" : "SANTA YSABEL", "loc" : [ -116.69635, 33.147579 ], "pop" : 1263, "state" : "CA" }
{ "_id" : "91941", "city" : "LA MESA", "loc" : [ -117.011541, 32.760431 ], "pop" : 42536, "state" : "CA" }
{ "_id" : "13833", "city" : "SANITARIA SPRING", "loc" : [ -75.790978, 42.195735 ], "pop" : 4777, "state" : "NY" }
{ "_id" : "95363", "city" : "PATTERSON", "loc" : [ -121.140732, 37.490592 ], "pop" : 13437, "state" : "CA" }
{ "_id" : "11590", "city" : "WESTBURY", "loc" : [ -73.57226, 40.755749 ], "pop" : 38026, "state" : "NY" }
{ "_id" : "95673", "city" : "RIO LINDA", "loc" : [ -121.445152, 38.689311 ], "pop" : 12756, "state" : "CA" }
{ "_id" : "14621", "city" : "ROCHESTER", "loc" : [ -77.604284, 43.183362 ], "pop" : 36055, "state" : "NY" }
{ "_id" : "11354", "city" : "FLUSHING", "loc" : [ -73.824142, 40.766722 ], "pop" : 51947, "state" : "NY" }
```

Aggregate by a compound key (state + city) and calculate sum of population
```
> db.zips.aggregate([
... {$match:{state:{$in:['NY', 'CA']}}},
... {$group:{ _id: {"state":"$state", "city":"$city"}, population: {$sum: "$pop"}, 'zips': {$push: "$_id"} }},
... {$limit: 10}])
{ "_id" : { "state" : "NY", "city" : "STERLINGTON" }, "population" : 3322, "zips" : [ "10974" ] }
{ "_id" : { "state" : "NY", "city" : "NEW YORK" }, "population" : 8190, "zips" : [ "10044" ] }
{ "_id" : { "state" : "NY", "city" : "DEWITTVILLE" }, "population" : 1159, "zips" : [ "14728" ] }
{ "_id" : { "state" : "NY", "city" : "HUDSON FALLS" }, "population" : 12866, "zips" : [ "12839" ] }
{ "_id" : { "state" : "NY", "city" : "RODMAN" }, "population" : 1097, "zips" : [ "13682" ] }
{ "_id" : { "state" : "CA", "city" : "ORINDA" }, "population" : 16883, "zips" : [ "94563" ] }
{ "_id" : { "state" : "CA", "city" : "LUDLOW" }, "population" : 383, "zips" : [ "92338" ] }
{ "_id" : { "state" : "NY", "city" : "ENDWELL" }, "population" : 46236, "zips" : [ "13760" ] }
{ "_id" : { "state" : "NY", "city" : "PORTER CORNERS" }, "population" : 1028, "zips" : [ "12859" ] }
{ "_id" : { "state" : "NY", "city" : "RIPLEY" }, "population" : 2876, "zips" : [ "14775" ] }
```

Remove cities from result that have a population less than 25000 and test it by sorting the result.
```
> db.zips.aggregate([
... {$match:{state:{$in:['NY', 'CA']}}},
... {$group:{ _id: {"state":"$state", "city":"$city"}, population: {$sum: "$pop"}, 'zips': {$push: "$_id"} }},
... {$match: {population: {$gte: 25000}}},
... {$sort: {population: 1}},
... {$limit: 10}])
{ "_id" : { "state" : "CA", "city" : "FORT ORD" }, "population" : 25009, "zips" : [ "93941" ] }
{ "_id" : { "state" : "CA", "city" : "MILL VALLEY" }, "population" : 27746, "zips" : [ "94941" ] }
{ "_id" : { "state" : "CA", "city" : "OAKLAND" }, "population" : 27930, "zips" : [ "94613", "94603" ] }
{ "_id" : { "state" : "CA", "city" : "SUISUN CITY" }, "population" : 31081, "zips" : [ "94585" ] }
{ "_id" : { "state" : "NY", "city" : "ALBANY" }, "population" : 31415, "zips" : [ "12208", "12210" ] }
{ "_id" : { "state" : "CA", "city" : "ORANGE" }, "population" : 31583, "zips" : [ "92669" ] }
{ "_id" : { "state" : "CA", "city" : "TORRANCE" }, "population" : 33933, "zips" : [ "90505" ] }
{ "_id" : { "state" : "CA", "city" : "SANTA CRUZ" }, "population" : 34287, "zips" : [ "95062" ] }
{ "_id" : { "state" : "NY", "city" : "ROCHESTER" }, "population" : 36055, "zips" : [ "14621" ] }
{ "_id" : { "state" : "NY", "city" : "BROOKLYN" }, "population" : 36852, "zips" : [ "11205" ] }
```

Calculate average of population. First test with the states CT and NJ.
```
> db.zips.aggregate([ {$match:{state:{$in:['CT', 'NJ']}}}, {$group:{ _id: {"state":"$state", "city":"$city"}, population: {$sum: "$pop"}, 'zips': {$push: "$_id"} }}, {$match: {population: {$gte: 25000}}}, {$group: {_id:"CT+NJ", avg: {$avg:"$population"}}}])
{ "_id" : "CT+NJ", "avg" : 38176.63636363636 }
```

Get average of pupulation from NY and CA
```
> db.zips.aggregate([
... {$match:{state:{$in:['NY', 'CA']}}},
... {$group:{ _id: {"state":"$state", "city":"$city"}, population: {$sum: "$pop"}, 'zips': {$push: "$_id"} }},
... {$match: {population: {$gte: 25000}}},
... {$group: {_id:"CA+NY", avg: {$avg:"$population"}}}])
{ "_id" : "CA+NY", "avg" : 44804.782608695656 }
```
