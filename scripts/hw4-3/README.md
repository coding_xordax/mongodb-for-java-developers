# hw4-3

download the provided `posts.json` or use it from repo

start mongo database: `docker-compose up -d`

connect to docker container: `docker exec -it mongo bash`

go to `hw4-3` directory: `cd scripts/hw4-3`

prepare database: `mongoimport --drop -d blog -c posts posts.json`

start mongo shell: `mongo`

* use command to add indexes
* use command to explain a query


For example:
```java
> return postsCollection.find()
                       .sort(Sorts.descending("date"))
                       .limit(limit)
                       .into(new ArrayList<Document>());
```

equals to query (with limit=10):
```
> db.posts.find().sort({date: -1}).limit(10).pretty()
```

use explain on this:
```
> db.posts.explain().find().sort({date: -1}).limit(10)
```

the query can be made faster by adding an index on `date` with desending order (-1)

add cursor:
```
> db.posts.ensureIndex( { date : -1 } )
```

use explain on this again:
```
> db.posts.explain().find().sort({date: -1}).limit(10)
```

Other mongo queries from java are:

```
> return postsCollection.find(Filters.eq("tags", tag))
                       .sort(Sorts.descending("date"))
                       .limit(10)
                       .into(new ArrayList<Document>());
```
equals to:

```
> db.posts.find({tags: {$eq: 'sudan'}}).sort({date: -1}).limit(10)
```

```
> Document post = postsCollection.find(Filters.eq("permalink", permalink)).first();
< return post;
```
equals to:

```
> db.posts.find({permalink: {$eq: 'sudan'}}).limit(10)
```

count documents (to test if query is correct)
```
> db.posts.find({tags: {$eq: 'sudan'}}).count()
```

pretty print some documents only a few short fields (by projection)
```
> db.posts.find({tags: {$eq: 'sudan'}},{_id:0,title:1,permalink:1}).limit(7).pretty()
```