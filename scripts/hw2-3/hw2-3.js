use students;

var skipStudentIds = [];
var gradesToRemove = [];

print("\nNumber of grades:")
db.grades.count();

db.grades.find( { type: "homework" } ).sort( { student_id: 1, score : 1 } ).forEach(
    function(grade) {
        if (!skipStudentIds.includes(grade.student_id)) {
            skipStudentIds.push(grade.student_id);
            gradesToRemove.push(grade._id);
        }
    }
);

gradesToRemove.forEach(
    function(gradeId) {
        print("\ndelete grade: " + gradeId);
        var result = db.grades.remove({_id : gradeId});
        print("\nresult: " + result);
    }
)

print("\n\nNumber of grades:")
db.grades.count()

print("\n\nDeleted grades:")
print(gradesToRemove.length)