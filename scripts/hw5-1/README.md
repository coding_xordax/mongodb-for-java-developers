# hw5-1

## prepare db

download the provided `posts.json` or use it from repo

start mongo database: `docker-compose up -d`

connect to docker container: `docker exec -it mongo bash`

go to `hw5-1` directory: `cd scripts/hw5-1`

prepare database: `mongoimport --drop -d blog -c posts posts.json`

start mongo shell: `mongo`

use database: `use blog`

## hints

Get example document:
```
> db.posts.find({}).limit(1).pretty()
```
```
{
	"_id": ObjectId("50ab0f8bbcf1bfe2536dc3f8"),
	"body": "Lorem ipsum dolor sit amet ...",
	"permalink": "TqoHkbHyUgLyCKWgPLqm",
	"author": "machine",
	"title": "US Constitution",
	"tags": ["trade",
	"fowl",
	"forecast",
	"pest",
	"professor",
	"willow",
	"rise",
	"brace",
	"ink",
	"road"],
	"comments": [{
		"body": "Lorem ipsum dolor sit amet ...",
		"email": "yXOfHoMo@JCIDTfDJ.com",
		"author": "Beckie Millington"
	},
	{
		"body": "Lorem ipsum dolor sit amet ...",
		"email": "yXOfHoMo@JCIDTfDJ.com",
		"author": "Beckie Millington"
	}],
	"date": ISODate("2012-11-20T05:05:15.229Z")
}
```


Get number of documents in posts collection
```
> db.posts.find().count()
1000
```
> there are 1000 documents


Get number of posts by author:
```
> db.posts.aggregate([{$project:{author:1}},{$group: {_id:"$author", "nr_of_posts": {$sum:1}}}]).pretty()
{ "_id" : "machine", "nr_of_posts" : 1000 }
```
> all 1000 posts are from author "machine"

Build up the query in steps:

Remove some fields and unwind comments
```
> db.posts.aggregate([{$project:{_id:0, body:0, tags:0, permalink:0}},{$unwind:"$comments"},{$limit:1}]).pretty()
{
        "author" : "machine",
        "title" : "US Constitution",
        "comments" : {
                "body" : "Lorem ipsum dolor sit amet ...",
                "email" : "LkvwxBUa@LOPSfuBf.com",
                "author" : "Tonia Surace"
        },
        "date" : ISODate("2012-11-20T05:05:15.229Z")
}
```

Create a projection to get only author of a comment
```
> db.posts.aggregate([{$project:{_id:0, body:0, tags:0, permalink:0}},{$unwind:"$comments"},{$project:{"comments.author": 1}},{$limit:10}]).pretty()
{ "comments" : { "author" : "Tonia Surace" } }
{ "comments" : { "author" : "Kim Xu" } }
{ "comments" : { "author" : "Ta Sikorski" } }
{ "comments" : { "author" : "Tamika Schildgen" } }
{ "comments" : { "author" : "Kayce Kenyon" } }
{ "comments" : { "author" : "Echo Pippins" } }
{ "comments" : { "author" : "Cody Strouth" } }
{ "comments" : { "author" : "Flora Duell" } }
{ "comments" : { "author" : "Toshiko Sabella" } }
{ "comments" : { "author" : "Gennie Ratner" } }
```

Count comments of a authors
```
> db.posts.aggregate([{$project:{_id:0, body:0, tags:0, permalink:0}},{$unwind:"$comments"},{$project:{"comments.author": 1}},{$group: {_id:"$comments.author", "nr_of_comments": {$sum:1}}}]).pretty()
{ "_id" : "Mariette Batdorf", "nr_of_comments" : 478 }
{ "_id" : "Osvaldo Hirt", "nr_of_comments" : 431 }
{ "_id" : "Devorah Smartt", "nr_of_comments" : 466 }
{ "_id" : "Karry Petrarca", "nr_of_comments" : 434 }
{ "_id" : "Vina Matsunaga", "nr_of_comments" : 448 }
{ "_id" : "Mikaela Meidinger", "nr_of_comments" : 443 }
{ "_id" : "Tambra Mercure", "nr_of_comments" : 465 }
{ "_id" : "Tonisha Games", "nr_of_comments" : 418 }
{ "_id" : "Verdell Sowinski", "nr_of_comments" : 459 }
{ "_id" : "Fleta Duplantis", "nr_of_comments" : 411 }
{ "_id" : "Vinnie Auerbach", "nr_of_comments" : 443 }
{ "_id" : "Demarcus Audette", "nr_of_comments" : 431 }
{ "_id" : "Bao Ziglar", "nr_of_comments" : 421 }
{ "_id" : "Synthia Labelle", "nr_of_comments" : 418 }
{ "_id" : "Beckie Millington", "nr_of_comments" : 428 }
{ "_id" : "Kayce Kenyon", "nr_of_comments" : 400 }
{ "_id" : "aimee Zank", "nr_of_comments" : 421 }
{ "_id" : "Malisa Jeanes", "nr_of_comments" : 464 }
{ "_id" : "Gwyneth Garling", "nr_of_comments" : 477 }
{ "_id" : "Denisha Cast", "nr_of_comments" : 450 }
```
Add sort by number of comments: Top 10 authors with fewest number of comments
```
> db.posts.aggregate([{$project:{_id:0, body:0, tags:0, permalink:0}},{$unwind:"$comments"},{$project:{"comments.author": 1}},{$group: {_id:"$comments.author", "nr_of_comments": {$sum:1}}},{$sort: {"nr_of_comments": 1}}, {$limit: 10}]).pretty()
{ "_id" : "Mariela Sherer", "nr_of_comments" : 387 }
{ "_id" : "Tawana Oberg", "nr_of_comments" : 396 }
{ "_id" : "Kayce Kenyon", "nr_of_comments" : 400 }
{ "_id" : "Maren Scheider", "nr_of_comments" : 401 }
{ "_id" : "Tamika Schildgen", "nr_of_comments" : 404 }
{ "_id" : "Kaila Deibler", "nr_of_comments" : 406 }
{ "_id" : "Zachary Langlais", "nr_of_comments" : 406 }
{ "_id" : "Jessika Dagenais", "nr_of_comments" : 406 }
{ "_id" : "Marcus Blohm", "nr_of_comments" : 408 }
{ "_id" : "Tandra Meadows", "nr_of_comments" : 410 }
```

Reverse sort: Most frequent author
```
> db.posts.aggregate([{$project:{_id:0, body:0, tags:0, permalink:0}},{$unwind:"$comments"},{$project:{"comments.author": 1}},{$group: {_id:"$comments.author", "nr_of_comments": {$sum:1}}},{$sort: {"nr_of_comments": -1}}, {$limit: 10}]).pretty()
{ "_id" : "Elizabet Kleine", "nr_of_comments" : 503 }
{ "_id" : "Carli Belvins", "nr_of_comments" : 480 }
{ "_id" : "Mariette Batdorf", "nr_of_comments" : 478 }
{ "_id" : "Gwyneth Garling", "nr_of_comments" : 477 }
{ "_id" : "Eugene Magdaleno", "nr_of_comments" : 475 }
{ "_id" : "Corliss Zuk", "nr_of_comments" : 472 }
{ "_id" : "Milan Mcgavock", "nr_of_comments" : 472 }
{ "_id" : "Leonida Lafond", "nr_of_comments" : 472 }
{ "_id" : "Dusti Lemmond", "nr_of_comments" : 471 }
{ "_id" : "Sadie Jernigan", "nr_of_comments" : 471 }
```

Simplyfy the query: The top author is:
```
> db.posts.aggregate([{$unwind:"$comments"},{$group: {_id:"$comments.author", "nr_of_comments": {$sum:1}}},{$sort: {"nr_of_comments": -1}}, {$limit: 1}]).pretty()
{ "_id" : "Elizabet Kleine", "nr_of_comments" : 503 }
```