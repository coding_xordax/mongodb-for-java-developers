use video

// clear data base from previous experiments
print("\ndelete movies")
db.movies.deleteMany({})

print("\nadd movies")
db.movies.insertOne({ "title": "Iron Man", "year": 2008, "imdb": "tt0371746" })
db.movies.insertOne({ "title": "Thor", "year": 2011, "imdb": "tt0800369" })
db.movies.insertOne({ "title": "Man of Steel", "year": 2013, "imdb": "tt0770828" })

print("\nget movies")
db.movies.find()