# hw5-3

## prepare db

download the provided `grades.json` or use it from repo

start mongo database: `docker-compose up -d`

connect to docker container: `docker exec -it mongo bash`

go to `hw5-3` directory: `cd scripts/hw5-3`

prepare database: `mongoimport --drop -d test -c grades grades.json`

start mongo shell: `mongo`

use database: `use test`

## hints

Get some information on the collection and documents
```
> db.grades.find().count()
280
```
```
> db.grades.find().limit(1).pretty()
{
        "_id" : ObjectId("50b59cd75bed76f46522c34e"),
        "student_id" : 0,
        "class_id" : 2,
        "scores" : [
                {
                        "type" : "exam",
                        "score" : 57.92947112575566
                },
                {
                        "type" : "quiz",
                        "score" : 21.24542588206755
                },
                {
                        "type" : "homework",
                        "score" : 68.1956781058743
                },
                {
                        "type" : "homework",
                        "score" : 67.95019716560351
                },
                {
                        "type" : "homework",
                        "score" : 18.81037253352722
                }
        ]
}
```

Unwind scores to get single documents
```
> db.grades.aggregate([
... {$unwind:"$scores"},
... {$limit: 10}
... ])
{ "_id" : ObjectId("50b59cd75bed76f46522c34e"), "student_id" : 0, "class_id" : 2, "scores" : { "type" : "exam", "score" : 57.92947112575566 } }
{ "_id" : ObjectId("50b59cd75bed76f46522c34e"), "student_id" : 0, "class_id" : 2, "scores" : { "type" : "quiz", "score" : 21.24542588206755 } }
{ "_id" : ObjectId("50b59cd75bed76f46522c34e"), "student_id" : 0, "class_id" : 2, "scores" : { "type" : "homework", "score" : 68.1956781058743 } }
{ "_id" : ObjectId("50b59cd75bed76f46522c34e"), "student_id" : 0, "class_id" : 2, "scores" : { "type" : "homework", "score" : 67.95019716560351 } }
{ "_id" : ObjectId("50b59cd75bed76f46522c34e"), "student_id" : 0, "class_id" : 2, "scores" : { "type" : "homework", "score" : 18.81037253352722 } }
{ "_id" : ObjectId("50b59cd75bed76f46522c34f"), "student_id" : 0, "class_id" : 28, "scores" : { "type" : "exam", "score" : 39.17749400402234 } }
{ "_id" : ObjectId("50b59cd75bed76f46522c34f"), "student_id" : 0, "class_id" : 28, "scores" : { "type" : "quiz", "score" : 78.44172815491468 } }
{ "_id" : ObjectId("50b59cd75bed76f46522c34f"), "student_id" : 0, "class_id" : 28, "scores" : { "type" : "homework", "score" : 20.81782269075502 } }
{ "_id" : ObjectId("50b59cd75bed76f46522c34f"), "student_id" : 0, "class_id" : 28, "scores" : { "type" : "homework", "score" : 70.44520452408949 } }
{ "_id" : ObjectId("50b59cd75bed76f46522c34f"), "student_id" : 0, "class_id" : 28, "scores" : { "type" : "homework", "score" : 50.66616327819226 } }
```

Ensure that only exams and homeworks are considered
```
> db.grades.aggregate([
... {$unwind:"$scores"},
... {$match: {"scores.type": {$in:['exam','homework']}}},
... {$limit: 10}
... ])
{ "_id" : ObjectId("50b59cd75bed76f46522c34e"), "student_id" : 0, "class_id" : 2, "scores" : { "type" : "exam", "score" : 57.92947112575566 } }
{ "_id" : ObjectId("50b59cd75bed76f46522c34e"), "student_id" : 0, "class_id" : 2, "scores" : { "type" : "homework", "score" : 68.1956781058743 } }
{ "_id" : ObjectId("50b59cd75bed76f46522c34e"), "student_id" : 0, "class_id" : 2, "scores" : { "type" : "homework", "score" : 67.95019716560351 } }
{ "_id" : ObjectId("50b59cd75bed76f46522c34e"), "student_id" : 0, "class_id" : 2, "scores" : { "type" : "homework", "score" : 18.81037253352722 } }
{ "_id" : ObjectId("50b59cd75bed76f46522c34f"), "student_id" : 0, "class_id" : 28, "scores" : { "type" : "exam", "score" : 39.17749400402234 } }
{ "_id" : ObjectId("50b59cd75bed76f46522c34f"), "student_id" : 0, "class_id" : 28, "scores" : { "type" : "homework", "score" : 20.81782269075502 } }
{ "_id" : ObjectId("50b59cd75bed76f46522c34f"), "student_id" : 0, "class_id" : 28, "scores" : { "type" : "homework", "score" : 70.44520452408949 } }
{ "_id" : ObjectId("50b59cd75bed76f46522c34f"), "student_id" : 0, "class_id" : 28, "scores" : { "type" : "homework", "score" : 50.66616327819226 } }
{ "_id" : ObjectId("50b59cd75bed76f46522c34f"), "student_id" : 0, "class_id" : 28, "scores" : { "type" : "homework", "score" : 53.84983118363991 } }
{ "_id" : ObjectId("50b59cd75bed76f46522c350"), "student_id" : 0, "class_id" : 5, "scores" : { "type" : "exam", "score" : 88.22950674232497 } }
```

Group to get average grade per student
```
> db.grades.aggregate([
... {$unwind:"$scores"},
... {$match: {"scores.type": {$in:['exam','homework']}}},
... {$group : {"_id" : {"student_id" : "$student_id", "class_id" : "$class_id"} , avg_student : {$avg: "$scores.score"}} },
... {$limit: 10}
... ])
{ "_id" : { "student_id" : 49, "class_id" : 29 }, "avg_student" : 22.281622173245808 }
{ "_id" : { "student_id" : 48, "class_id" : 14 }, "avg_student" : 75.40764716796762 }
{ "_id" : { "student_id" : 48, "class_id" : 10 }, "avg_student" : 28.04028866642439 }
{ "_id" : { "student_id" : 46, "class_id" : 20 }, "avg_student" : 62.06947485161923 }
{ "_id" : { "student_id" : 46, "class_id" : 3 }, "avg_student" : 33.64734411613102 }
{ "_id" : { "student_id" : 45, "class_id" : 29 }, "avg_student" : 46.01117661122032 }
{ "_id" : { "student_id" : 45, "class_id" : 6 }, "avg_student" : 53.63529012612206 }
{ "_id" : { "student_id" : 45, "class_id" : 25 }, "avg_student" : 50.71689974165258 }
{ "_id" : { "student_id" : 45, "class_id" : 5 }, "avg_student" : 38.61414845027249 }
{ "_id" : { "student_id" : 44, "class_id" : 21 }, "avg_student" : 54.71295284245051 }
```

Group again to get average grade of a class
```
> db.grades.aggregate([
... {$unwind:"$scores"},
... {$match: {"scores.type": {$in:['exam','homework']}}},
... {$group : {"_id" : {"student_id" : "$student_id", "class_id" : "$class_id"} , avg_student : {$avg: "$scores.score"}} },
... {$group: {"_id" : "$_id.class_id", avg_class: {$avg : "$avg_student"}}},
... {$limit: 10}
... ])
{ "_id" : 9, "avg_class" : 55.56861693456625 }
{ "_id" : 4, "avg_class" : 52.655415610658586 }
{ "_id" : 12, "avg_class" : 40.62345969481146 }
{ "_id" : 8, "avg_class" : 41.30388381000019 }
{ "_id" : 16, "avg_class" : 53.45833539362425 }
{ "_id" : 0, "avg_class" : 50.64317695848949 }
{ "_id" : 30, "avg_class" : 42.71200726236121 }
{ "_id" : 28, "avg_class" : 41.59824801397288 }
{ "_id" : 24, "avg_class" : 53.610345978016596 }
{ "_id" : 21, "avg_class" : 45.415393233594564 }
```

Get classes with the lowest average by sorting 
ascending
```
> db.grades.aggregate([
... {$unwind:"$scores"},
... {$match: {"scores.type": {$in:['exam','homework']}}},
... {$group : {"_id" : {"student_id" : "$student_id", "class_id" : "$class_id"} , "avg_student" : {$avg: "$scores.score"}} },
... {$group: {"_id" : "$_id.class_id", avg_class: {$avg : "$avg_student"}}},
... {$sort: {"avg_class" : 1}},
... {$limit: 10}
... ])
{ "_id" : 2, "avg_class" : 37.61742117387635 }
{ "_id" : 12, "avg_class" : 40.62345969481146 }
{ "_id" : 8, "avg_class" : 41.30388381000019 }
{ "_id" : 28, "avg_class" : 41.59824801397288 }
{ "_id" : 30, "avg_class" : 42.71200726236121 }
{ "_id" : 18, "avg_class" : 43.40692681712815 }
{ "_id" : 22, "avg_class" : 43.47760542089679 }
{ "_id" : 25, "avg_class" : 43.998820032401284 }
{ "_id" : 21, "avg_class" : 45.415393233594564 }
{ "_id" : 15, "avg_class" : 47.22009466630579 }
```

Get classes with the highest average by sorting descending
```
> db.grades.aggregate([
... {$unwind:"$scores"},
... {$match: {"scores.type": {$in:['exam','homework']}}},
... {$group : {"_id" : {"student_id" : "$student_id", "class_id" : "$class_id"} , "avg_student" : {$avg: "$scores.score"}} },
... {$group: {"_id" : "$_id.class_id", avg_class: {$avg : "$avg_student"}}},
... {$sort: {"avg_class" : -1}},
... {$limit: 10}
... ])
{ "_id" : 1, "avg_class" : 64.50642324269175 }
{ "_id" : 5, "avg_class" : 58.08448767613548 }
{ "_id" : 20, "avg_class" : 57.6309834548989 }
{ "_id" : 26, "avg_class" : 56.06918278769095 }
{ "_id" : 9, "avg_class" : 55.56861693456625 }
{ "_id" : 14, "avg_class" : 55.36017373346247 }
{ "_id" : 24, "avg_class" : 53.610345978016596 }
{ "_id" : 16, "avg_class" : 53.45833539362425 }
{ "_id" : 13, "avg_class" : 52.738286239952366 }
{ "_id" : 4, "avg_class" : 52.655415610658586 }
```