# hw4-4

download the provided `sysprofile.json` or use it from repo

start mongo database: `docker-compose up -d`

connect to docker container: `docker exec -it mongo bash`

go to `hw4-4` directory: `cd scripts/hw4-4`

prepare database: `mongoimport --drop -d m101 -c sysprofile sysprofile.json`

start mongo shell: `mongo`
