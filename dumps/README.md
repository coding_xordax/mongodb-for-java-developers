# Restore database

Restore mongo database using `mongorestore` command:

```sh
mongorestore dump
```

## Steps

1. copy `dump` to `scripts` directory inside of the directory with the `docker-compose`-file.

1. start mongodb
```sh
$ docker-compose up -d
```

1. check if mongo is started
```sh
$ docker-compose ps
```

1. connect to mongo
```sh
$ docker exec -it mongo bash
```

1. restore dump

```sh
$ mongorestore /dumps/[selected-dump]/dump
```

Example
```sh
$ cd scripts
$ mongorestore /dumps/week2/dump
root@f3e0d5f4d048:/scripts/hw1-1# mongorestore dump 
2017-06-19T19:26:53.705+0000    preparing collections to restore from
2017-06-19T19:26:53.709+0000    reading metadata for m101.funnynumbers from dump/m101/funnynumbers.metadata.json
2017-06-19T19:26:54.516+0000    restoring m101.funnynumbers from dump/m101/funnynumbers.bson
2017-06-19T19:26:54.523+0000    reading metadata for m101.hw1 from dump/m101/hw1.metadata.json
2017-06-19T19:26:55.574+0000    restoring m101.hw1 from dump/m101/hw1.bson
2017-06-19T19:26:55.580+0000    no indexes to restore
2017-06-19T19:26:55.580+0000    finished restoring m101.funnynumbers (100 documents)
2017-06-19T19:26:55.584+0000    no indexes to restore
2017-06-19T19:26:55.584+0000    finished restoring m101.hw1 (1 document)
2017-06-19T19:26:55.584+0000    done
```

1. start mongo shell
```sh
mongo
```

1. execute some queries
Example
```sh
use m101
db.hw1.findOne()
```