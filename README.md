# MongoDB for Java Developers #

Sample applications to work with a MongoDB


## Content

* 0_installation

    setup mongodb, mongoshell and execute some commands

* 1_week1

    * spark framework intro (not apache spark!)
    * freemarker intro (template engine)

* 2_week2

    * crud operations

* 3_week3

   * schema design

* 4_week4

    * Indexes
    * Monitoring
    * Performance

* 5_week5

    * The pipeline

* 5_week5

    * The aggregation pipeline

* 6_week6

    * Drivers
    * Replication
    * Sharding

* 7_final

    * Final exam

* scripts

    * mounted as docker volume
    * contains some mongo scripts and sample datas for homeworks

* dumps

    * mounted as docker volume
    * contains some mongo dumps for homeworks


## Docker MongoDB setup


### setup

docker-compose.yml

    * a single mongodb setup using docker
    * used port: 127.0.0.1
    * able to connect with mongoProc, mongo shell and other mongo clients


### start 

```sh
docker-compose up -d
docker exec -it mongo bash
```


## Docker MongoDB replica setup

see [6_week6 > docker](./6_week6/hw6-5/README.md)


## More

* https://github.com/jrgcubano/mongoeduc
* https://github.com/sirmes/mongo_courses
* http://sonalikothari.com/
* https://github.com/oscerd/mongoDB-course-M101J
* https://github.com/YuriyGuts/mongodb-java-course-m101j


## MongoDB tools

https://mongobooster.com/