# hw6-5

If MongoProc is not able to resolve the hostnames configured in the MongoProc settings, then
add following entry to `/etc/hosts`

```
127.0.0.1       localhost.localdomain   mongodb-p mongodb-s1 mongodb-s2
```

And add follow entries to MongoProc settings:
```
server name: mongodb-p
server address: 127.0.0.1
server port: 27017

server name: mongodb-s1
server address: 127.0.0.1
server port: 27018

server name: mongodb-s2
server address: 127.0.0.1
server port: 27019
```

start mongo database: `docker-compose up -d`

connect to docker container: `docker exec -it mongodb-p bash`

start mongo shell: `mongo --port 27017`

configurate the replica set
```js
rs.initiate({
  "_id": "m101",
  "version": 1,
  "members" : [
   {"_id": 1, "host": "mongodb-p:27017"},
   {"_id": 2, "host": "mongodb-s1:27017"},
   {"_id": 3, "host": "mongodb-s2:27017"}
  ]
 })
```

get replica set status:
```js
rs0:PRIMARY> rs.status()
{
        "set" : "rs0",
        "date" : ISODate("2017-07-10T16:46:25.194Z"),
        "myState" : 1,
        "term" : NumberLong(1),
        "heartbeatIntervalMillis" : NumberLong(2000),
        "optimes" : {
                "lastCommittedOpTime" : {
                        "ts" : Timestamp(1499705176, 1),
                        "t" : NumberLong(1)
                },
                "appliedOpTime" : {
                        "ts" : Timestamp(1499705176, 1),
                        "t" : NumberLong(1)
                },
                "durableOpTime" : {
                        "ts" : Timestamp(1499705176, 1),
                        "t" : NumberLong(1)
                }
        },
        "members" : [
                {
                        "_id" : 1,
                        "name" : "mongodb-p:27017",
                        "health" : 1,
                        "state" : 1,
                        "stateStr" : "PRIMARY",
                        "uptime" : 466,
                        "optime" : {
                                "ts" : Timestamp(1499705176, 1),
                                "t" : NumberLong(1)
                        },
                        "optimeDate" : ISODate("2017-07-10T16:46:16Z"),
                        "infoMessage" : "could not find member to sync from",
                        "electionTime" : Timestamp(1499705105, 1),
                        "electionDate" : ISODate("2017-07-10T16:45:05Z"),
                        "configVersion" : 1,
                        "self" : true
                },
                {
                        "_id" : 2,
                        "name" : "mongodb-s1:27017",
                        "health" : 1,
                        "state" : 2,
                        "stateStr" : "SECONDARY",
                        "uptime" : 95,
                        "optime" : {
                                "ts" : Timestamp(1499705176, 1),
                                "t" : NumberLong(1)
                        },
                        "optimeDurable" : {
                                "ts" : Timestamp(1499705176, 1),
                                "t" : NumberLong(1)
                        },
                        "optimeDate" : ISODate("2017-07-10T16:46:16Z"),
                        "optimeDurableDate" : ISODate("2017-07-10T16:46:16Z"),
                        "lastHeartbeat" : ISODate("2017-07-10T16:46:23.355Z"),
                        "lastHeartbeatRecv" : ISODate("2017-07-10T16:46:24.981Z"),
                        "pingMs" : NumberLong(0),
                        "syncingTo" : "mongodb-p:27017",
                        "configVersion" : 1
                },
                {
                        "_id" : 3,
                        "name" : "mongodb-s2:27017",
                        "health" : 1,
                        "state" : 2,
                        "stateStr" : "SECONDARY",
                        "uptime" : 95,
                        "optime" : {
                                "ts" : Timestamp(1499705176, 1),
                                "t" : NumberLong(1)
                        },
                        "optimeDurable" : {
                                "ts" : Timestamp(1499705176, 1),
                                "t" : NumberLong(1)
                        },
                        "optimeDate" : ISODate("2017-07-10T16:46:16Z"),
                        "optimeDurableDate" : ISODate("2017-07-10T16:46:16Z"),
                        "lastHeartbeat" : ISODate("2017-07-10T16:46:23.355Z"),
                        "lastHeartbeatRecv" : ISODate("2017-07-10T16:46:24.451Z"),
                        "pingMs" : NumberLong(0),
                        "syncingTo" : "mongodb-p:27017",
                        "configVersion" : 1
                }
        ],
        "ok" : 1
}
```