#!/bin/bash

docker-compose stop
docker rm mongodb-p mongodb-s1 mongodb-s2
docker volume prune
